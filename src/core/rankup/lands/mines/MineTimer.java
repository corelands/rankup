package core.rankup.lands.mines;

import core.rankup.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class MineTimer extends BukkitRunnable {

    @Override
    public void run() {
        for(Mine mine : Core.getInstance().getMineManager().getMines())  {
            mine.resetMine();
        }
        Bukkit.broadcastMessage("§6[Mines] §7All mines have been reset.");
    }

}
