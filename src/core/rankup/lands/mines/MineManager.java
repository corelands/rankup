package core.rankup.lands.mines;

import core.rankup.lands.Core;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public class MineManager {

    private HashMap<String, Mine> mines;
    private LinkedList<Mine> sortedMines;

    public MineManager() {
        this.mines = new HashMap<>();
        this.sortedMines = new LinkedList<>();
    }

    public MineManager loadMines() {
        YamlConfiguration minesFile = YamlConfiguration.loadConfiguration(new File(Core.getInstance().getDataFolder(), "mines.yml"));
        for(String mineWorldName : minesFile.getConfigurationSection("").getKeys(false)) {
            for(String mineName : minesFile.getConfigurationSection(mineWorldName).getKeys(false)) {
                Mine mine = new Mine(mineName, mineWorldName);
                mine.load();
                this.mines.put(mineName.toLowerCase(), mine);
                this.sortedMines.add(mine);
            }
        }
        return this;
    }

    public void addMine(Mine mine) {
        mines.put(mine.getMineName().toLowerCase(), mine);
        sortedMines.add(mine);
    }

    public LinkedList<Mine> getMines() {
        return this.sortedMines;
    }

    public Mine getMineByName(String mine) {
        return mines.get(mine.toLowerCase());
    }

}
