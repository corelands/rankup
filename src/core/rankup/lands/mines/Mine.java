package core.rankup.lands.mines;

import core.rankup.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Mine {

    private File minesFile;
    private YamlConfiguration mines;

    private String mineName, minePath;
    private World world;
    private Block down, up;
    private Random random;
    private int probability;
    private HashMap<Integer, Material> blockList;

    private List<Block> blocksArea;

    public Mine(String mineName, String world) {
        this.mineName = mineName;
        this.random = new Random();
        this.blockList = new HashMap<>();
        this.blocksArea = new ArrayList<>();
        this.world = Bukkit.getWorld(world);
        this.minePath = world + "." + mineName + ".";
        minesFile = new File(Core.getInstance().getDataFolder(), "mines.yml");
        mines = YamlConfiguration.loadConfiguration(minesFile);
    }

    public void create(Block down, Block up) {
        this.down = down;
        this.up = up;
        this.mines.set(this.minePath + "downBlock.x", down.getX());
        this.mines.set(this.minePath + "downBlock.y", down.getY());
        this.mines.set(this.minePath + "downBlock.z", down.getZ());
        this.mines.set(this.minePath + "upBlock.x", up.getX());
        this.mines.set(this.minePath + "upBlock.y", up.getY());
        this.mines.set(this.minePath + "upBlock.z", up.getZ());
        saveMine();
        Core.getInstance().getMineManager().addMine(this);
        load();
    }

    public void resetMine() {
        if(probability > 0) {
            for (Block block : blocksArea) {
                block.setType(blockList.get(random.nextInt(probability)));
            }
        }
    }

    public void addBlock(String material, int percent) {
        try {
            Material blockType = Material.getMaterial(material);
            this.mines.set(this.minePath + "blocks."+ blockType, percent);
            reloadBlocks();
            saveMine();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private int getInt(String path) {
        return this.mines.getInt(this.minePath + path);
    }

    public void reloadBlocks() {
        for (String blocks : mines.getConfigurationSection(this.minePath + "blocks").getKeys(false)) {
            int percent = this.mines.getInt(this.minePath + "blocks." + blocks);
            for (int i = this.probability; i < this.probability + percent; i++) {
                blockList.put(i, Material.valueOf(blocks));
            }
            this.probability += percent;
        }
    }

    public void load() {
        this.down = this.world.getBlockAt(getInt("downBlock.x"), getInt("downBlock.y"), getInt("downBlock.z"));
        this.up = this.world.getBlockAt(getInt("upBlock.x"), getInt("upBlock.y"), getInt("upBlock.z"));
        for(int x = down.getX(); x <= up.getX(); x++) {
            for(int y = down.getY(); y <= up.getY(); y++) {
                for(int z = down.getZ(); z <= up.getZ(); z++) {
                    blocksArea.add(world.getBlockAt(x, y, z));
                }
            }
        }
        try {
            reloadBlocks();
        } catch (Exception exc) {
            System.out.println("Mine #" + mineName + " without blocks.");
        }
    }

    public String getMineName() {
        return this.mineName;
    }

    private void saveMine() {
        try {
            mines.save(minesFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
