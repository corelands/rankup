package core.rankup.lands.listeners;

import core.rankup.lands.Core;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class EventPlayerChat implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        e.setCancelled(true);
        RankupPlayer rankupPlayer = Core.getInstance().getCachedPlayers().get(e.getPlayer());
        String message = rankupPlayer.getRole().chatColor() ? e.getMessage().replaceAll("&", "§") : e.getMessage();
        Bukkit.broadcastMessage(rankupPlayer.getRole().getTag() + rankupPlayer.getRank().getColor() + rankupPlayer.getRank().getTag() + " " + rankupPlayer.getColor() + rankupPlayer.getName() + "§7: " + message);
    }

}
