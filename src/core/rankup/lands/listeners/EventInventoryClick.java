package core.rankup.lands.listeners;

import core.rankup.lands.Core;
import core.rankup.lands.kits.Kit;
import core.rankup.lands.spawners.SpawnerManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class EventInventoryClick implements Listener {

    @EventHandler
    public void inventoryClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        if (e.getInventory().equals(Core.getInstance().getKitManager().getSelectionGUI())) {
            kitSelection(e, player);
            e.setCancelled(true);
        } else if(e.getInventory().equals(Core.getInstance().getKitManager().getMembersGUI())) {
            Kit kit = Core.getInstance().getKitManager().getMemberKitBySlot(e.getSlot());
            membersKits(e, player, kit);
            e.setCancelled(true);
        } else if(e.getInventory().equals(Core.getInstance().getKitManager().getVipsGUI())) {
            Kit kit = Core.getInstance().getKitManager().getVipKitBySlot(e.getSlot());
            membersKits(e, player, kit);
            e.setCancelled(true);
        } else if(e.getInventory().equals(Core.getInstance().getSpawnerManager().getSpawnerInventory())) {
            spawners(e, player);
            e.setCancelled(true);
        }
    }

    private void kitSelection(InventoryClickEvent e, Player player) {
        if(e.getSlot() == 12) {
            player.openInventory(Core.getInstance().getKitManager().getMembersGUI());
        } else if(e.getSlot() == 14) {
            player.openInventory(Core.getInstance().getKitManager().getVipsGUI());
        }
    }

    private void membersKits(InventoryClickEvent e, Player player, Kit kit) {
        if(kit != null) {
            for(ItemStack item : kit.getItems()) {
                player.getInventory().addItem(item);
            }
            player.sendMessage("§6[Kits] §7You received the kit " + kit.getGuiName() + "§7.");
            player.closeInventory();
        }
    }

    private void spawners(InventoryClickEvent e, Player player) {
        SpawnerManager spawnerManager = Core.getInstance().getSpawnerManager();
        Material material = Core.getInstance().getSpawnerManager().getMaterialBySlot(e.getSlot());
        if(material != null) {
            spawnerManager.createSpawner(player.getLocation().getBlock().getLocation(), e.getSlot(), player);
        }
    }

}
