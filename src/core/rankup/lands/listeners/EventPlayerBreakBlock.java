package core.rankup.lands.listeners;

import core.rankup.lands.Core;
import core.rankup.lands.spawners.Spawner;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class EventPlayerBreakBlock implements Listener {

    @EventHandler
    public void onPlayerBreakBlock(BlockBreakEvent e) {
        RankupPlayer rankupPlayer = Core.getInstance().getCachedPlayers().get(e.getPlayer());

        if(rankupPlayer.isMineMode()) {
            if(rankupPlayer.getItemInHand().getType() == Material.STICK) {
                rankupPlayer.setMineBlockDown(e.getBlock());
                rankupPlayer.sendMessage("§6[Mines] §7Down block.");
            } else if(rankupPlayer.getItemInHand().getType() == Material.BLAZE_ROD) {
                rankupPlayer.setMineBlockUp(e.getBlock());
                rankupPlayer.sendMessage("§6[Mines] §7Up block.");
            }
        }

        for(Spawner spawner : Core.getInstance().getSpawnerManager().getSpawners()) {
            if(spawner.getLocation().getWorld().getBlockAt(spawner.getLocation()).equals(e.getBlock())) {
                e.setCancelled(true);
            }
        }

    }

}
