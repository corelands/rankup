package core.rankup.lands.listeners;

import core.rankup.lands.Core;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class EventEntityDeath implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        LivingEntity entity = e.getEntity();

        if (entity.getType() != EntityType.PLAYER) {
            Core.getInstance().getEntityStack().kill(entity);
        }
    }

}
