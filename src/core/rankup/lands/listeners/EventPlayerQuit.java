package core.rankup.lands.listeners;

import core.rankup.lands.Core;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventPlayerQuit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        e.setQuitMessage(null);
        RankupPlayer rankupPlayer = Core.getInstance().getCachedPlayers().get(player);
        rankupPlayer.disconnect();
    }

}
