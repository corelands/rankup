package core.rankup.lands.listeners;

import core.rankup.lands.Core;
import core.rankup.lands.spawners.Spawner;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class EventPlayerInteract implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            for(Spawner spawner : Core.getInstance().getSpawnerManager().getSpawners()) {
                if(spawner.getLocation().getWorld().getBlockAt(spawner.getLocation()).equals(e.getClickedBlock())) {
                    e.getPlayer().openInventory(Core.getInstance().getSpawnerManager().getSettingsGUI(e.getPlayer(), spawner));
                }
            }
        }
    }

}
