package core.rankup.lands.listeners;

import core.rankup.lands.Core;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EventPlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        RankupPlayer rankupPlayer = new RankupPlayer(e.getPlayer());
        Core.getInstance().getCachedPlayers().put(e.getPlayer(), rankupPlayer);
    }

}
