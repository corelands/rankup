package core.rankup.lands.controllers.scoreboard;

import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class RankScoreboard {

    private Scoreboard board;
    private Objective obj;
    private RankupPlayer player;

    public RankScoreboard(RankupPlayer player) {
        this.player = player;
        this.board = Bukkit.getScoreboardManager().getNewScoreboard();
        this.obj = board.registerNewObjective("Rankup", "dummy");
        this.obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.obj.setDisplayName("§6§lRankUP");
    }

    public void setScoreboard() {

        this.obj.getScore("§c ").setScore(9);
        this.obj.getScore(" §7Rank:").setScore(8);

        Team rank = board.registerNewTeam("rank");
        rank.addEntry("  §7Current: ");
        rank.setSuffix("§e" + this.player.getRank().getName());
        this.obj.getScore("  §7Current: ").setScore(7);

        Team nextRank = board.registerNewTeam("nextRank");
        nextRank.addEntry("  §7Next: ");
        if(this.player.nextRank() != null) {
            nextRank.setSuffix("§e" + this.player.nextRank().getName());
        } else {
            nextRank.setSuffix("§eNone");
        }
        this.obj.getScore("  §7Next: ").setScore(6);

        Team progress = board.registerNewTeam("progress");
        progress.addEntry("  §7Progress: ");
        progress.setSuffix("§7[" + this.player.rankProgress() + "§7]");
        this.obj.getScore("  §7Progress: ").setScore(5);

        this.obj.getScore("§b ").setScore(4);

        Team money = board.registerNewTeam("money");
        money.addEntry(" §7Money: ");
        money.setSuffix("§e" + player.getFormattedMoney());
        this.obj.getScore(" §7Money: ").setScore(3);

        Team clan = board.registerNewTeam("clan");
        clan.addEntry(" §7Clan: ");
        clan.setSuffix("§e" + player.getClan().getName());
        this.obj.getScore(" §7Clan: ").setScore(2);

        this.obj.getScore("§a ").setScore(1);

        this.obj.getScore("§7corelands.net").setScore(0);

        this.player.setScoreboard(this.board);
    }

}
