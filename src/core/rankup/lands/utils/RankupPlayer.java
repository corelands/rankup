package core.rankup.lands.utils;

import core.rankup.lands.Core;
import core.rankup.lands.clans.Clan;
import core.rankup.lands.controllers.scoreboard.RankScoreboard;
import core.rankup.lands.ranks.Rank;
import core.rankup.lands.ranks.Role;
import net.minecraft.server.v1_13_R2.BlockKelp;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

public class RankupPlayer {

    private File usersFile;
    private YamlConfiguration users;

    private OfflinePlayer player;

    private Role role;
    private Rank rank;
    private Clan clan;

    private boolean mineMode;
    private Block mineBlockDown, mineBlockUp;

    private String userPath;

    private double money;

    public RankupPlayer(String playerName) {
        //OfflinePlayer
        this.player = Bukkit.getOfflinePlayer(playerName);
        setup(playerName.toLowerCase());
        updateScoreboard();
    }
    public RankupPlayer(Player player) {
        //OnlinePlayer
        this.player = player;
        this.clan = new Clan();
        this.mineMode = false;
        setup(player.getName().toLowerCase());
        updateScoreboard();
    }

    /*
        @Player
     */

    private boolean isValid() {
        return this.player != null;
    }

    public String getName() {
        return this.player.getName();
    }

    public String getColor() {
        return "§" + get("color");
    }

    /*
        @Settings
     */

    private void saveUser() {
        try {
            users.save(usersFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Object get(String path) {
        return users.get(this.player.getName().toLowerCase() + "." + path);
    }

    private void setup(String playerName) {
        usersFile = new File(Core.getInstance().getDataFolder(), "users.yml");
        users = YamlConfiguration.loadConfiguration(usersFile);
        userPath = playerName.toLowerCase() + ".";
        if (users.get(playerName.toLowerCase()) == null) {
            users.set(userPath + "rank", Core.getInstance().getRankManager().getRanks().get(0).getName());
            users.set(userPath + "color", "7");
            users.set(userPath + "coins", 0);
            users.set(userPath + "tokens", 0);
            users.set(userPath + "role", "MEMBER");
            users.set(userPath + "clan", "None");
            saveUser();
        }
        this.rank = Core.getInstance().getRankManager().getRankByName(users.getString(userPath + "rank"));
        this.role = Role.valueOf(users.getString(userPath + "role"));
        this.money = users.getDouble(userPath + "coins");
        if(this.player.isOnline()) {
            new RankScoreboard(this).setScoreboard();
        }
    }

    public void disconnect() {
        saveUser();
        Core.getInstance().getCachedPlayers().remove(this.player.getPlayer());
    }

    /*
        @Rankup
     */

    public Rank getRank() {
        return this.rank;
    }

    public Rank nextRank() {
        return (this.rank.getPower()+1 < Core.getInstance().getRankManager().getRanks().size()) ? Core.getInstance().getRankManager().getRanks().get(this.rank.getPower()+1) : null;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public void rankup() {
        Player onlinePlayer = this.player.getPlayer();

        this.money -= nextRank().getPrice();
        onlinePlayer.sendTitle("§6§lRankUP", "§7You ranked up to §e" + nextRank().getName(), 5, 20, 5);
        onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1f, 1f);
        this.rank = nextRank();
        updateUser();
    }

    public String rankProgress() {
        if(nextRank() != null) {
            String progressBar = "§a";
            int progress = (int) (this.money / nextRank().getPrice() * 10);
            if (progress > 10) progress = 10;
            for (int i = 0; i < progress; i++) {
                progressBar += "|";
            }
            progressBar += "§7";
            for (int i = 0; i < 10 - progress; i++) {
                progressBar += "|";
            }
            return progressBar;
        } else {
            return "§eMAX";
        }
    }


    /*
        @Clans
     */

    public Clan getClan() {
        return this.clan;
    }

    /*
        @Roles
     */

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
        updateUser();
    }

    /*
        @Money
     */

    public double getMoney() {
        return this.money;
    }

    public String getFormattedMoney() {
        if (this.money / 1000 / 1000 / 1000 >= 1) {
        return new DecimalFormat("0.0").format(this.money / 1000 / 1000 / 1000) + " B";
        } else if (this.money / 1000 / 1000 >= 1) {
            return new DecimalFormat("0.0").format(this.money / 1000 / 1000) + " M";
        } else if (this.money / 1000 >= 1) {
            return new DecimalFormat("0.0").format(this.money / 1000) + " K";
        } else return new DecimalFormat("0.00").format(this.money);
    }

    public void addMoney(double amount) {
        this.money += amount;
        updateUser();
    }

    public void removeMoney(double amount) {
        this.money -= amount;
        updateUser();
    }

    public void setMoney(double amount) {
        this.money = amount;
        updateUser();
    }

    /*
        @Mine
     */

    public boolean isMineMode() {
        return this.mineMode;
    }

    public void setMineMode(boolean value) {
        this.mineMode = value;
    }

    public Block getMineBlockDown() {
        return this.mineBlockDown;
    }

    public Block getMineBlockUp() {
        return this.mineBlockUp;
    }

    public void setMineBlockDown(Block mineBlockDown) {
        this.mineBlockDown = mineBlockDown;
    }

    public void setMineBlockUp(Block mineBlockUp) {
        this.mineBlockUp = mineBlockUp;
    }

    /*
        @Default Methods
     */

    public void sendMessage(String message) {
        this.player.getPlayer().sendMessage(message);
    }

    public void setScoreboard(Scoreboard scoreboard) {
        this.player.getPlayer().setScoreboard(scoreboard);
    }

    public ItemStack getItemInHand() {
        return this.player.getPlayer().getItemInHand();
    }

    public World getWorld() {
        return this.player.getPlayer().getWorld();
    }

    /*
        @Updates
     */

    private void updateUser() {
        users.set(userPath + "role", this.role.toString());
        users.set(userPath + "clan", this.clan.getName());
        users.set(userPath + "rank", this.rank.getName());
        users.set(userPath + "coins", this.money);
        saveUser();
        updateScoreboard();
    }

    private void updateScoreboard() {
        if(this.rank != null) {
            this.player.getPlayer().getScoreboard().getTeam("rank").setSuffix("§e" + this.rank.getName());
            if(this.nextRank() != null) {
                this.player.getPlayer().getScoreboard().getTeam("nextRank").setSuffix("§e" + nextRank().getName());
            } else {
                this.player.getPlayer().getScoreboard().getTeam("nextRank").setSuffix("§eNone");
            }
            this.player.getPlayer().getScoreboard().getTeam("progress").setSuffix("§7[" + rankProgress() + "§7]");
            this.player.getPlayer().getScoreboard().getTeam("money").setSuffix("§e" + getFormattedMoney());
            this.player.getPlayer().getScoreboard().getTeam("clan").setSuffix("§e" + this.clan.getName());
        }
    }

}
