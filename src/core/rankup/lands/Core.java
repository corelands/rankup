package core.rankup.lands;

import core.rankup.lands.commands.*;
import core.rankup.lands.kits.KitManager;
import core.rankup.lands.listeners.*;
import core.rankup.lands.mines.MineManager;
import core.rankup.lands.mines.MineTimer;
import core.rankup.lands.ranks.RankManager;
import core.rankup.lands.spawners.SpawnerManager;
import core.rankup.lands.spawners.SpawnerTimer;
import core.rankup.lands.spawners.stack.EntityStack;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;


public class Core extends JavaPlugin {

    private static Core instance;
    private HashMap<Player, RankupPlayer> cachedPlayers;
    private RankManager rankManager;
    private MineManager mineManager;
    private KitManager kitManager;
    private SpawnerManager spawnerManager;
    private EntityStack entityStack;

    public void onEnable() {
        instance = this;
        this.cachedPlayers = new HashMap<>();
        this.rankManager = new RankManager().loadRanks();
        this.mineManager = new MineManager().loadMines();
        this.kitManager = new KitManager().loadKits();
        this.spawnerManager = new SpawnerManager().loadSpawners();
        this.entityStack = new EntityStack();
        new MineTimer().runTaskTimer(this, 0, 20*60*5);
        new SpawnerTimer().runTaskTimer(this, 0, 20*5);
        this.registerCommands();
        this.registerListeners();
        this.reloadPlayers();
    }

    private void reloadPlayers() {
        for(Player all : Bukkit.getOnlinePlayers()) {
            RankupPlayer rankupPlayer = new RankupPlayer(all);
            this.cachedPlayers.put(all, rankupPlayer);
        }
    }

    public void onDisable() {

    }

    private void registerCommands() {
        getCommand("rankup").setExecutor(new CmdRankup());
        getCommand("money").setExecutor(new CmdMoney());
        getCommand("clearchat").setExecutor(new CmdClearChat());
        getCommand("setrole").setExecutor(new CmdSetRole());
        getCommand("mine").setExecutor(new CmdMineCreator());
        getCommand("kit").setExecutor(new CmdKit());
        getCommand("spawners").setExecutor(new CmdSpawners());
    }

    private void registerListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new EventPlayerChat(), this);
        pluginManager.registerEvents(new EventPlayerJoin(), this);
        pluginManager.registerEvents(new EventPlayerQuit(), this);
        pluginManager.registerEvents(new EventPlayerBreakBlock(), this);
        pluginManager.registerEvents(new EventInventoryClick(), this);
        pluginManager.registerEvents(new EventEntityDeath(), this);
        pluginManager.registerEvents(new EventPlayerInteract(), this);
    }

    public RankManager getRankManager() {
        return this.rankManager;
    }

    public MineManager getMineManager() {
        return this.mineManager;
    }

    public KitManager getKitManager() {
        return this.kitManager;
    }

    public SpawnerManager getSpawnerManager() {
        return this.spawnerManager;
    }

    public EntityStack getEntityStack() {
        return this.entityStack;
    }

    public HashMap<Player, RankupPlayer> getCachedPlayers() {
        return this.cachedPlayers;
    }

    public static Core getInstance() {
        return instance;
    }

}
