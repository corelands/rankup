package core.rankup.lands.ranks;

public enum Role {

    OWNER("§4[Owner] ", true),
    ADMIN("§c[Admin] ", true),
    MOD("§2[Mod] ", true),
    HELPER("§b[Helper] ", true),
    BUILDER("§6[Builder] ", true),
    KNIGHT("§d[Knight] ", true),
    HERO("§3[Hero] ", true),
    IMMORTAL("§a[Immortal] ", true),
    LEGEND("§e[Legend] ", true),
    MEMBER("", false);

    private String tag;
    private boolean color;

    Role(String tag, boolean color) {
        this.tag = tag;
        this.color = color;
    }

    public String getTag() {
        return this.tag;
    }

    public boolean chatColor() {
        return this.color;
    }

}
