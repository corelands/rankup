package core.rankup.lands.ranks;

public class Rank {

    private String name, tag, color;
    private int power;
    private double price;

    Rank(String name, String tag, int power, String color, double price) {
        this.name = name;
        this.tag = tag;
        this.power = power;
        this.color = color;
        this.price = price;
    }

    public String getColor() {
        return "§" + this.color;
    }

    public double getPrice() {
        return this.price;
    }

    public String getName() {
        return this.name;
    }

    public String getTag() {
        return this.getColor() + this.tag;
    }

    public int getPower() {
        return this.power;
    }

    public String getFormattedPrice() {
        return this.price + "";
    }

}
