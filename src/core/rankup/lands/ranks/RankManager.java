package core.rankup.lands.ranks;

import core.rankup.lands.Core;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public class RankManager {

    private HashMap<String, Rank> ranks;
    private LinkedList<Rank> sortedRanks;

    public RankManager() {
        this.ranks = new HashMap<>();
        this.sortedRanks = new LinkedList<>();
    }

    public RankManager loadRanks() {
        YamlConfiguration ranks = YamlConfiguration.loadConfiguration(new File(Core.getInstance().getDataFolder(), "ranks.yml"));
        int power = -1;
        for(String rankName : ranks.getConfigurationSection("").getKeys(false)) {
            power++;
            String tag = ranks.getString(rankName + ".tag"),
            name = ranks.getString(rankName + ".name"),
            color = ranks.getString(rankName + ".color");
            double price = ranks.getDouble(rankName + ".price");
            Rank rank = new Rank(name, tag, power, color, price);
            this.ranks.put(rankName.toLowerCase(), rank);
            this.sortedRanks.add(rank);
        }
        return this;
    }

    public LinkedList<Rank> getRanks() {
        return this.sortedRanks;
    }

    public Rank getRankByName(String rank) {
        return ranks.get(rank.toLowerCase());
    }

}
