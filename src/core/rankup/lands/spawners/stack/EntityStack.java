package core.rankup.lands.spawners.stack;

import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import net.minecraft.server.v1_13_R2.NBTTagString;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

public class EntityStack {

    public void kill(LivingEntity livingEntity) {
        String displayName = livingEntity.getCustomName();
        int amount = getAmount(displayName);

        livingEntity.setHealth(0);

        if (amount <= 1) {
            return;
        }

        amount--;

        if(amount > 0) {
            LivingEntity dupEntity = (LivingEntity) livingEntity.getWorld().spawnEntity(livingEntity.getLocation(), livingEntity.getType());
            dupEntity.setCustomName("§6§l" + amount);
            dupEntity.setCustomNameVisible(true);
            freezeEntity(dupEntity);
        } else {
            livingEntity.remove();
        }
    }

    public boolean stack(LivingEntity target, LivingEntity stack) {
        if (target.getType() != stack.getType()) {
            return false;
        }

        int amountTarget = getAmount(target.getCustomName());

        if (!stackedEntity(target) || !stackedEntity(stack)) {
            return false;
        }

        target.remove();

        stack.setCustomName("§6§l" + (amountTarget + getAmount(stack.getCustomName())));

        return true;
    }

    private int getAmount(String displayName) {
        if (displayName == null) {
            return -1;
        }

        return Integer.parseInt(displayName.replaceAll("§6§l", ""));
    }

    private boolean stackedEntity(LivingEntity entity) {
        return getAmount(entity.getCustomName()) != -1;
    }

    private void freezeEntity(Entity entity){
        net.minecraft.server.v1_13_R2.Entity nmsEn = ((CraftEntity) entity).getHandle();
        NBTTagCompound compound = new NBTTagCompound();
        nmsEn.c(compound);
        compound.set("AttributeName", new NBTTagString("generic.knockbackResistance"));
        compound.set("Name", new NBTTagString("generic.knockbackResistance"));
        compound.set("Amount", new NBTTagInt(1));
        nmsEn.f(compound);
        nmsEn.c(compound);
        compound.set("AttributeName", new NBTTagString("generic.attackDamage"));
        compound.set("Name", new NBTTagString("generic.attackDamage"));
        compound.set("Amount", new NBTTagInt(0));
        nmsEn.f(compound);
        nmsEn.c(compound);
        compound.set("AttributeName", new NBTTagString("generic.movementSpeed"));
        compound.set("Name", new NBTTagString("generic.movementSpeed"));
        compound.set("Amount", new NBTTagInt(0));
        nmsEn.f(compound);
    }

}
