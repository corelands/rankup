package core.rankup.lands.spawners;

import core.rankup.lands.Core;
import core.rankup.lands.api.Hologram;
import core.rankup.lands.api.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Skull;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

public class SpawnerManager {

    private Inventory spawnersInventory;
    private HashMap<Integer, Material> materialSlots;
    private HashMap<Integer, String> ownerSlots;
    private HashMap<Integer, SpawnerType> spawnerSlots;
    private LinkedList<Spawner> spawners;

    public SpawnerManager() {
        this.materialSlots = new HashMap<>();
        this.ownerSlots = new HashMap<>();
        this.spawnerSlots = new HashMap<>();
        this.spawners = new LinkedList<>();
    }

    public SpawnerManager loadSpawners() {
        spawnersInventory = Bukkit.createInventory(null, 9 * 6, "§eSpawners");
        for(SpawnerType spawnerType : SpawnerType.values()) {
            this.spawnersInventory.setItem(spawnerType.getSlot(), spawnerType.getHead());
            this.materialSlots.put(spawnerType.getSlot(), spawnerType.getHead().getType());
            this.ownerSlots.put(spawnerType.getSlot(), ((SkullMeta) spawnerType.getHead().getItemMeta()).getOwner());
            this.spawnerSlots.put(spawnerType.getSlot(), spawnerType);
        }

        YamlConfiguration spawnersFile = YamlConfiguration.loadConfiguration(new File(Core.getInstance().getDataFolder(), "spawners.yml"));
        for(String spawnerName : spawnersFile.getConfigurationSection("").getKeys(false)) {
            Spawner spawner = new Spawner(spawnerName);
            spawner.load();
            this.spawners.add(spawner);
        }

        return this;
    }

    public void createSpawner(Location location, int slot, Player player) {
        String name = spawnerSlots.get(slot).getHead().getItemMeta().getDisplayName();
        location.getWorld().getBlockAt(location).setType(getMaterialBySlot(slot));
        Skull skull = (Skull) location.getWorld().getBlockAt(location).getState();
        skull.setOwner(getOwnerBySlot(slot));
        skull.update();
        location.add(0.5, -1, 0.5);
        new Hologram(location, "§7Status: §aON");
        location.add(0, 0.25, 0);
        new Hologram(location, "§7Owner: §e" + player.getName());
        location.add(0, 0.25, 0);
        new Hologram(location, "§7Level: §e1");
        location.add(0, 0.25, 0);
        new Hologram(location, name + " §eSpawner");

        File spawnersFile = new File(Core.getInstance().getDataFolder(), "spawners.yml");
        YamlConfiguration spawners = YamlConfiguration.loadConfiguration(spawnersFile);
        String spawnName = "Spawner" + spawners.getConfigurationSection("").getKeys(false).size();
        spawners.set(spawnName + ".location.world", location.getWorld().getName());
        spawners.set(spawnName + ".location.x", skull.getX());
        spawners.set(spawnName + ".location.y", skull.getY());
        spawners.set(spawnName + ".location.z", skull.getZ());
        spawners.set(spawnName + ".owner", player.getName());
        spawners.set(spawnName + ".level", 1);
        spawners.set(spawnName + ".type", getSpawnerTypeBySlot(slot).name());

        try {
            spawners.save(spawnersFile);
        } catch(IOException e) {
            e.printStackTrace();
        }

        Spawner spawner = new Spawner(spawnName);
        spawner.load();
        this.spawners.add(spawner);
    }

    public Material getMaterialBySlot(int slot) {
        if(materialSlots.containsKey(slot)) {
            return materialSlots.get(slot);
        }
        return  null;
    }

    public String getOwnerBySlot(int slot) {
        if(ownerSlots.containsKey(slot)) {
            return ownerSlots.get(slot);
        }
        return  null;
    }

    public SpawnerType getSpawnerTypeBySlot(int slot) {
        if(spawnerSlots.containsKey(slot)) {
            return spawnerSlots.get(slot);
        }
        return  null;
    }

    public Inventory getSettingsGUI(Player player, Spawner spawner) {
        Inventory inventory = Bukkit.createInventory(player, 27, spawner.getSpawnerType().getHead().getItemMeta().getDisplayName());
        inventory.setItem(11, new ItemCreator(Material.EXPERIENCE_BOTTLE).getItem());
        inventory.setItem(13, spawner.getSpawnerType().getHead());
        inventory.setItem(15, new ItemCreator(Material.RED_STAINED_GLASS_PANE).getItem());
        return inventory;
    }

    public LinkedList<Spawner> getSpawners() {
        return this.spawners;
    }

    public Inventory getSpawnerInventory() {
        return this.spawnersInventory;
    }

}
