package core.rankup.lands.spawners;

import core.rankup.lands.Core;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import net.minecraft.server.v1_13_R2.NBTTagString;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import java.io.File;
import java.util.Random;

public class Spawner {

    private File spawnersFile;
    private YamlConfiguration spawners;

    private String spawnerPath, owner;
    private SpawnerType spawnerType;
    private int level;
    private Location location;

    public Spawner(String spawner) {
        this.spawnerPath = spawner + ".";
        spawnersFile = new File(Core.getInstance().getDataFolder(), "spawners.yml");
        spawners = YamlConfiguration.loadConfiguration(spawnersFile);
    }

    public void load() {
        this.location = new Location(Bukkit.getWorld(spawners.getString(spawnerPath + "location.world")), spawners.getDouble(spawnerPath + "location.x"), spawners.getDouble(spawnerPath + "location.y"), spawners.getDouble(spawnerPath + "location.z"));
        this.owner = spawners.getString(spawnerPath + "owner");
        this.spawnerType = SpawnerType.valueOf(spawners.getString(spawnerPath + "type").toUpperCase());
        this.level = spawners.getInt(spawnerPath + "level");
    }

    public void spawnEntity() {
        Random random = new Random();
        LivingEntity livingEntity;
        int rndX = random.nextInt(4);
        int rndY = random.nextInt(4);
        if(random.nextBoolean()) {
            livingEntity = (LivingEntity) this.location.getWorld().spawnEntity(this.location.clone().add(rndX+1, 0, rndY+1), spawnerType.getEntityType());
        } else {
            livingEntity = (LivingEntity) this.location.getWorld().spawnEntity(this.location.clone().add(-rndX-1, 0, -rndY-1), spawnerType.getEntityType());
        }
        livingEntity.setCustomName("§6§l" + this.level);
        livingEntity.setCustomNameVisible(true);

        freezeEntity(livingEntity);

        for(Entity nearby : livingEntity.getNearbyEntities(5, 5, 5)) {
            try {
                LivingEntity livingNearby = (LivingEntity) nearby;
                if (Core.getInstance().getEntityStack().stack(livingEntity, livingNearby)) {
                    break;
                }
            } catch(Exception exc) {

            }
        }
    }

    public SpawnerType getSpawnerType() {
        return spawnerType;
    }

    public String getOwner() {
        return this.owner;
    }

    public Location getLocation() {
        return this.location;
    }

    private void freezeEntity(Entity entity){
        net.minecraft.server.v1_13_R2.Entity nmsEn = ((CraftEntity) entity).getHandle();
        NBTTagCompound compound = new NBTTagCompound();
        nmsEn.c(compound);
        compound.set("AttributeName", new NBTTagString("generic.knockbackResistance"));
        compound.set("Name", new NBTTagString("generic.knockbackResistance"));
        compound.set("Amount", new NBTTagInt(1));
        nmsEn.f(compound);
        nmsEn.c(compound);
        compound.set("AttributeName", new NBTTagString("generic.attackDamage"));
        compound.set("Name", new NBTTagString("generic.attackDamage"));
        compound.set("Amount", new NBTTagInt(0));
        nmsEn.f(compound);
        nmsEn.c(compound);
        compound.set("AttributeName", new NBTTagString("generic.movementSpeed"));
        compound.set("Name", new NBTTagString("generic.movementSpeed"));
        compound.set("Amount", new NBTTagInt(0));
        nmsEn.f(compound);
    }

}
