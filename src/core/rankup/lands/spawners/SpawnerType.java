package core.rankup.lands.spawners;

import core.rankup.lands.api.ItemCreator;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

public enum SpawnerType {

    DROWNED(10, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Drowned").setName("§bDrowned").getItem(), EntityType.DROWNED),
    VINDICATOR(11, new ItemCreator(Material.PLAYER_HEAD).setHead("Vindicator").setName("§8Vindicator").getItem(), EntityType.VINDICATOR),
    WITCH(12, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Witch").setName("§5Witch").getItem(), EntityType.WITCH),
    COW(13, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Cow").setName("§eCow").getItem(), EntityType.COW),
    GOLEM(14, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Golem").setName("§7Iron Golem").getItem(), EntityType.IRON_GOLEM),
    SPIDER(15, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Spider").setName("§cSpider").getItem(), EntityType.SPIDER),
    SLIME(16, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Slime").setName("§aSlime").getItem(), EntityType.SLIME),
    BLAZE(19, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Blaze").setName("§6Blaze").getItem(), EntityType.BLAZE),
    ENDERMAN(20, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Enderman").setName("§0Enderman").getItem(), EntityType.ENDERMAN),
    PIG_ZOMBIE(21, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_PigZombie").setName("§dPig Zombie").getItem(), EntityType.PIG_ZOMBIE),
    LAVA_SLIME(22, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_LavaSlime").setName("§4Magma").getItem(), EntityType.MAGMA_CUBE),
    PIG(23, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Pig").setName("§dPig").getItem(), EntityType.PIG),
    CHICKEN(24, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Chicken").setName("§fChicken").getItem(), EntityType.CHICKEN),
    SHEEP(25, new ItemCreator(Material.PLAYER_HEAD).setHead("MHF_Sheep").setName("§fSheep").getItem(), EntityType.SHEEP),
    ZOMBIE(30, new ItemCreator(Material.ZOMBIE_HEAD).setName("§aZombie").getItem(), EntityType.ZOMBIE),
    SKELETON(31, new ItemCreator(Material.SKELETON_SKULL).setName("§7Skeleton").getItem(), EntityType.SKELETON),
    CREEPER(32, new ItemCreator(Material.CREEPER_HEAD).setName("§2Creeper").getItem(), EntityType.CREEPER);

    private ItemStack head;
    private int slot;
    private EntityType entityType;

    SpawnerType(int slot, ItemStack head, EntityType entityType) {
        this.head = head;
        this.slot = slot;
        this.entityType = entityType;
    }

    public int getSlot() {
        return this.slot;
    }

    public ItemStack getHead() {
        return this.head;
    }

    public EntityType getEntityType() {
        return this.entityType;
    }

}
