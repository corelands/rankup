package core.rankup.lands.spawners;

import core.rankup.lands.Core;
import org.bukkit.scheduler.BukkitRunnable;

public class SpawnerTimer extends BukkitRunnable {

    @Override
    public void run() {
        for(Spawner spawner : Core.getInstance().getSpawnerManager().getSpawners()) {
            spawner.spawnEntity();
        }
    }

}
