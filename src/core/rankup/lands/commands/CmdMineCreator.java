package core.rankup.lands.commands;

import core.rankup.lands.Core;
import core.rankup.lands.mines.Mine;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdMineCreator implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String string, String[] args) {
        Player player = (Player) commandSender;
        RankupPlayer rankupPlayer = Core.getInstance().getCachedPlayers().get(player);
        if (commandSender.isOp()) {
            if (args.length == 0) {
                if (!rankupPlayer.isMineMode()) {
                    rankupPlayer.setMineMode(true);
                    rankupPlayer.sendMessage("§6[Mines] §7You are in mines mode now.");
                } else {
                    rankupPlayer.setMineMode(false);
                    rankupPlayer.sendMessage("§6[Mines] §7You are in normal mode now.");
                }
            } else if (args.length == 1) {
                if(args[0].equals("reset")) {
                    for(Mine mine : Core.getInstance().getMineManager().getMines()) {
                        mine.resetMine();
                    }
                    Bukkit.broadcastMessage("§6[Mines] §7All mines have been reset.");
                }
            }else if (args.length == 2) {
                if (args[0].equals("create")) {
                    String mineName = args[1].toLowerCase();
                    Mine mine = new Mine(mineName, rankupPlayer.getWorld().getName());
                    mine.create(rankupPlayer.getMineBlockDown(), rankupPlayer.getMineBlockUp());
                    rankupPlayer.sendMessage("§6[Mines] §7Mine §e" + args[1] + " §7created.");
                }
            } else if(args.length == 4) {
                if(args[0].equals("add")) {
                    String mine = args[1].toLowerCase();
                    String material = args[2].toUpperCase();
                    int percent = Integer.parseInt(args[3]);
                    Core.getInstance().getMineManager().getMineByName(mine).addBlock(material, percent);
                    rankupPlayer.sendMessage("§6[Mines] §7You added §e" + material + " §7to the mine §e" + mine + " §7with §e" + percent + "§7%");
                }
            }
        }
        return false;
    }
}
