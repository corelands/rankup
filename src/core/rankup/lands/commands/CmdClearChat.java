package core.rankup.lands.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdClearChat implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String string, String[] args) {
        if(commandSender.isOp()) {
            for(int i = 0; i < 100; i++) {
                Bukkit.broadcastMessage(" ");
            }
            Bukkit.broadcastMessage("§6[Chat] §7The chat was clean.");
        }
        return false;
    }
}
