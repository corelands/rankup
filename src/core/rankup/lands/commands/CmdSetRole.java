package core.rankup.lands.commands;

import core.rankup.lands.Core;
import core.rankup.lands.ranks.Role;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSetRole implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String string, String[] args) {
        Player player = (Player) commandSender;
        RankupPlayer rankupPlayer = Core.getInstance().getCachedPlayers().get(player);

        if (args.length == 2) {
            Player tryPlayer = Bukkit.getOfflinePlayer(args[0]).getPlayer();
            if(Core.getInstance().getCachedPlayers().containsKey(tryPlayer)) {
                String roleName = args[1].toUpperCase();
                try {
                    Role role = Role.valueOf(roleName);
                    Core.getInstance().getCachedPlayers().get(tryPlayer).setRole(role);
                    Bukkit.broadcastMessage("§6[Server] §e" + tryPlayer.getName() + " §7is now " + role.getTag());
                } catch (Exception exc) {
                    rankupPlayer.sendMessage("§6[Role] §7Role not found.");
                }
            } else {
                rankupPlayer.sendMessage("§6[Role] §7This player isn't online.");
            }
        }

        return false;
    }
}
