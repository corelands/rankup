package core.rankup.lands.commands;

import core.rankup.lands.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdKit implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        Player player = (Player) commandSender;

        player.openInventory(Core.getInstance().getKitManager().getSelectionGUI());

        return false;
    }
}
