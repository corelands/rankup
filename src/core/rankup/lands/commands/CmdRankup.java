package core.rankup.lands.commands;

import core.rankup.lands.Core;
import core.rankup.lands.ranks.Rank;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdRankup implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String string, String[] args) {
        Player player = (Player) commandSender;
        RankupPlayer rankupPlayer = Core.getInstance().getCachedPlayers().get(player);
        if(rankupPlayer.nextRank() != null) {
            Rank nextRank = rankupPlayer.nextRank();
            double money = rankupPlayer.getMoney();
            double rankupCost = nextRank.getPrice();

            if (rankupCost <= money) {
                rankupPlayer.rankup();
            } else {
                rankupPlayer.sendMessage("§6[RankUP] §7Next rank price: §e" + nextRank.getFormattedPrice());
                rankupPlayer.sendMessage("§6[RankUP] §7Money need to rankup: §e" + (rankupCost - money));
            }
        } else {
            rankupPlayer.sendMessage("§6[RankUP] §7You are already on the last rank.");
        }

        return false;
    }
}
