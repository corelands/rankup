package core.rankup.lands.commands;

import core.rankup.lands.Core;
import core.rankup.lands.utils.RankupPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdMoney implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String string, String[] args) {
        Player player = (Player) commandSender;
        RankupPlayer rankupPlayer = Core.getInstance().getCachedPlayers().get(player);

        if(args.length == 0) {
            rankupPlayer.sendMessage("§6[Balance] §7You have §e" + rankupPlayer.getFormattedMoney() + " §7coins.");
        } else if(args.length == 1) {
            Player tryPlayer = Bukkit.getOfflinePlayer(args[0]).getPlayer();
            if(Core.getInstance().getCachedPlayers().containsKey(tryPlayer)) {
                rankupPlayer.sendMessage("§6[Balance] §e" + args[0]  + "'s §7money: §e" + Core.getInstance().getCachedPlayers().get(tryPlayer).getFormattedMoney() + " §7coins.");
            } else {
                rankupPlayer.sendMessage("§6[Balance] §7This player isn't online.");
            }
        } else if(args.length == 3) {
            if(args[0].equals("set") && commandSender.isOp()) {
                Player tryPlayer = Bukkit.getOfflinePlayer(args[1]).getPlayer();
                if(Core.getInstance().getCachedPlayers().containsKey(tryPlayer)) {
                    double coins = Double.parseDouble(args[2]);
                    Core.getInstance().getCachedPlayers().get(tryPlayer).setMoney(Double.parseDouble(args[2]));
                    rankupPlayer.sendMessage("§6[Balance] §7You set §e" + tryPlayer.getName() + "§7's money to §e" + coins + " §7coins.");
                } else {
                    rankupPlayer.sendMessage("§6[Balance] §7This player isn't online.");
                }
            }
        }

        return false;
    }

}
