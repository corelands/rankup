package core.rankup.lands.kits;

import core.rankup.lands.Core;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Kit {

    private File kitsFile;
    private YamlConfiguration kits;

    private String kitName, kitPath, guiName;
    private List<String> permissions;
    private long delay;
    private boolean vip;
    private int profileId;

    private ItemStack profile;

    private List<ItemStack> kitItems;

    public Kit(String kitName) {
        kitsFile = new File(Core.getInstance().getDataFolder(), "kits.yml");
        kits = YamlConfiguration.loadConfiguration(kitsFile);
        this.kitName = kitName;
        this.kitPath = "kits." + this.kitName + ".";
        this.kitItems = new ArrayList<>();
        this.permissions = new ArrayList<>();
        load();
    }

    public void load() {
        this.delay = kits.getLong(kitPath + "delay");
        this.guiName = kits.getString(kitPath + "profile.name").replaceAll("&", "§");
        this.permissions = kits.getStringList(kitPath + "permissions");
        this.vip = kits.getBoolean(kitPath + "vip");
        this.profileId = kits.getInt(kitPath + "profile.id");
        ItemStack itemStack = new ItemStack(Material.valueOf(kits.getString(kitPath + "profile.material")));
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(this.guiName);
        itemMeta.setLore(listColorDebug(kits.getStringList(kitPath + "profile.description")));
        itemStack.setItemMeta(itemMeta);

        for(String item : kits.getConfigurationSection(kitPath + "items").getKeys(false)) {
            String itemPath = kitPath + "items." + item + ".";
            ItemStack kitItem = new ItemStack(Material.valueOf(item), kits.getInt(itemPath  + "amount"));
            if(kits.getConfigurationSection(itemPath + "enchantments") != null) {
                for (String enchant : kits.getConfigurationSection(itemPath + "enchantments").getKeys(false)) {
                    kitItem.addEnchantment(Enchantment.getByName(enchant), kits.getInt(itemPath + ".enchantments." + enchant));
                }
            }
            this.kitItems.add(kitItem);
        }

        this.profile = itemStack;
    }

    public String getGuiName() {
        return this.guiName;
    }

    public List<ItemStack> getItems() {
        return this.kitItems;
    }

    public ItemStack getProfile() {
        return this.profile;
    }

    public int getProfileId() {
        return this.profileId;
    }

    public boolean isVip() {
        return this.vip;
    }

    private void saveKit() {
        try {
            kits.save(kitsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> listColorDebug(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i).replaceAll("&", "§"));
        }
        return list;
    }

}
