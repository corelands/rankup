package core.rankup.lands.kits;

import core.rankup.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public class KitManager {

    private HashMap<String, Kit> kits;
    private HashMap<Integer, Kit> memberKits;
    private HashMap<Integer, Kit> vipKits;
    private LinkedList<Kit> sortedKits;

    private Inventory selectionGUI, vipsGUI, membersGUI;

    public KitManager() {
        this.kits = new HashMap<>();
        this.sortedKits = new LinkedList<>();
        this.memberKits = new HashMap<>();
        this.vipKits = new HashMap<>();
    }

    public KitManager loadKits() {
        YamlConfiguration kitsFile = YamlConfiguration.loadConfiguration(new File(Core.getInstance().getDataFolder(), "kits.yml"));
        for(String kitName : kitsFile.getConfigurationSection("kits").getKeys(false)) {
            Kit kit = new Kit(kitName);
            this.kits.put(kitName.toLowerCase(), kit);
            this.sortedKits.add(kit);
        }
        this.loadGUIs(kitsFile);
        return this;
    }

    public Inventory getSelectionGUI() {
        return this.selectionGUI;
    }

    public Inventory getVipsGUI() {
        return this.vipsGUI;
    }

    public Inventory getMembersGUI() {
        return this.membersGUI;
    }

    private void loadGUIs(YamlConfiguration kitsFile) {
        this.selectionGUI = Bukkit.createInventory(null, 9*3, "§6Kits");
        this.membersGUI = Bukkit.createInventory(null, 9*3, "§6Kits");
        this.vipsGUI = Bukkit.createInventory(null, 9*3, "§6Kits");

        for(String area : kitsFile.getConfigurationSection("gui").getKeys(false)) {
            String path = "gui." + area + ".";
            ItemStack itemStack = new ItemStack(Material.valueOf(kitsFile.getString(path + "material")));
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName(kitsFile.getString(path + "name").replaceAll("&", "§"));
            itemStack.setItemMeta(itemMeta);
            selectionGUI.setItem(kitsFile.getInt(path + "pos"), itemStack);
        }

        for(Kit kit : getKits()) {
            if(kit.isVip()) {
                this.vipsGUI.setItem(kit.getProfileId(), kit.getProfile());
                this.vipKits.put(kit.getProfileId(), kit);
            } else {
                System.out.println(kit.getProfileId() + "");
                this.membersGUI.setItem(kit.getProfileId(), kit.getProfile());
                this.memberKits.put(kit.getProfileId(), kit);
            }
        }

    }

    public Kit getMemberKitBySlot(int slot) {
        if(memberKits.containsKey(slot)) {
            return memberKits.get(slot);
        }
        return null;
    }

    public Kit getVipKitBySlot(int slot) {
        if(vipKits.containsKey(slot)) {
            return vipKits.get(slot);
        }
        return null;
    }

    private LinkedList<Kit> getKits() {
        return this.sortedKits;
    }

    public Kit getKitByName(String kit) {
        return this.kits.get(kit.toLowerCase());
    }

}
